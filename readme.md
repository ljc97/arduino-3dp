# Arduino 3DP Monitoring

## Objectives

* Record metrics about the physical properties of a 3d printer
  * Temperature of stepper drivers
  * Acceleration
  * Voltage/Current
    * Heated Bed
    * Steppers
* Send metrics to MQTT server via onboard WIFI

## Hardware

* Board - ESP32
* Temperature Sensors - DS18B20 (Onewire)
* Acceleromter - GY-521
* Voltage/Current - TBD

## Progress

* Onewire communication with temp sensors setup and working
* NTP setup
* MQTT Event publishing as JSON for Temperature
