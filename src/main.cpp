#include <DallasTemperature.h>
#include <OneWire.h>
#include <ArduinoJson.h>
#include "WiFi.h"
#include "PubSubClient.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

#include "Config.h"

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

int num_temp_sensors = 0;
DeviceAddress* temp_sensor_addresses;
char** temp_sensor_addresses_string;

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
WiFiUDP wifiUDP;
NTPClient ntpClient(wifiUDP);


char* address_as_string(DeviceAddress address) {
  char* address_string = (char*) malloc(sizeof(char) * 32);
  sprintf(address_string, "%02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X", address[0], address[1], address[2], address[3], address[4], address[5], address[6], address[7]);
  return address_string;
}


void send_message(JsonDocument& event, char* topic)
{
  char buffer[512];
  size_t n = serializeJson(event, buffer);
//  mqttClient.publish(topic, buffer, n);
}


void read_temp_sensor(int sensor_index) {
  // Read sensor at given index
  // Construct event including address + temp
  // Write out as JSON
  float temperature = sensors.getTempC(temp_sensor_addresses[sensor_index]);
  StaticJsonDocument<180> event;
  char* address_string = temp_sensor_addresses_string[sensor_index];
  event["address"] = address_string;
  if (temperature == DEVICE_DISCONNECTED_C) {
    // error temp
    event["error"] = "Device Disconnected";
  }
  else {
    // valid temp
    event["temperature"] = temperature;
  }
  event["timestamp"] = ntpClient.getEpochTime();
  serializeJson(event, Serial);
  Serial.println();
  send_message(event, MQTT_TOPIC_TEMP);
}

void setup_temp_sensors(void) {
  // Scan for OneWire Temp sensors
  sensors.begin();
  delay(1000);

  // Update overall device count
  num_temp_sensors = sensors.getDeviceCount();


  StaticJsonDocument<180> event;
  JsonArray event_addresses = event.createNestedArray("addresses");
  // get all sensor addresses -> array
  temp_sensor_addresses = new DeviceAddress[num_temp_sensors];
  temp_sensor_addresses_string = (char**) malloc(sizeof(char*) * num_temp_sensors);
  DeviceAddress current_address;
  for (int index = 0; index < num_temp_sensors; index++) {
    sensors.getAddress(current_address, index);
    memcpy(temp_sensor_addresses[index], current_address, sizeof(temp_sensor_addresses[index]));
    char* address_string = address_as_string(current_address);
    temp_sensor_addresses_string[index] = address_string;
    event_addresses.add(address_string);
  }
  // Publish init event with number of sensors, addresses, datetime
  event["num_temp_sensors"] = num_temp_sensors;
  event["timestamp"] = ntpClient.getEpochTime();
  serializeJson(event, Serial);
  Serial.println();
  send_message(event, MQTT_TOPIC_TEMP);
}


void setup(void)
{
  // Setup Serial
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PSK);

  Serial.print("Connecting to "); Serial.println(SSID);
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print('.');
    delay(500);
  }

  Serial.print(F("Connected. My IP address is: "));
  Serial.println(WiFi.localIP());

  mqttClient.setServer(MQTT_SERVER, 1883);
  ntpClient.begin();
  while(!ntpClient.update()) {
    ntpClient.forceUpdate();
  }
  setup_temp_sensors();
  
}

void loop(void)
{
  while(!ntpClient.update()) {
    ntpClient.forceUpdate();
  }
  // Send Request for Temp to all devices on bus
  sensors.requestTemperatures();

  // Delay until values are ready - read accell inbetween?
  //  delay(1000);

  // Loop over each device
  for (int index = 0; index < num_temp_sensors; index++) {
    read_temp_sensor(index);
  }

}



